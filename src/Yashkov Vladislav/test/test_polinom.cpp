#include "Polinom.h"
#include <gtest.h>

TEST(TPolinom, can_create_polinom)
{
	int monom[][2] = { { 1, 2 } };
	ASSERT_NO_THROW(TPolinom polinom(monom, 1););
}

TEST(TPolinom, can_create_one_elem_polinom)
{
	int monom[][2] = { { 1, 2 } };
	TPolinom polinom(monom, 1);
	TMonom tMon(1, 2);
	EXPECT_TRUE(tMon == *polinom.GetMonom());
}

TEST(TPolinom, can_create_two_elems_polinom)
{
	const int size = 2;
	int monoms[][2] = { { 1, 2 }, { 3, 4 } };
	TPolinom polinom(monoms, size);
	TMonom tMons[size];
	for (int i = 0; i < size; i++)
		tMons[i] = TMonom(monoms[i][0], monoms[i][1]);
	for (int i = 0; i < size; i++, polinom.GoNext())
		EXPECT_TRUE(tMons[i] == *polinom.GetMonom());
}

TEST(TPolinom, can_compare_the_same_polynoms)
{
	const int size = 3;
	int monoms[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 } };
	TPolinom polinom1(monoms, size);
	TPolinom polinom2(monoms, size);
	EXPECT_TRUE(polinom1 == polinom2);
}

TEST(TPolinom, can_compare_the_different_polynoms)
{
	const int size = 3;
	int monoms1[][2] = { { 1, 3 }, { 2, 4 }, { 2, 100 } };
	int monoms2[][2] = { { 2, 5 }, { 6, 9 }, { 6, 25 } };
	TPolinom polinom1(monoms1, size);
	TPolinom polinom2(monoms2, size);
	EXPECT_FALSE(polinom1 == polinom2);
}



TEST(TPolinom, can_copy_polinoms)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolinom polinom1(mon, size);

	TPolinom polinom2 = polinom1;

	EXPECT_TRUE(polinom1 == polinom2);
}

TEST(TPolinom, can_assign_polynoms)
{
	const int size = 2;
	int mon[][2] = { { 5, 3 }, { 2, 4 } };
	TPolinom polinom1(mon, size);
	TPolinom polinom2;

	polinom2 = polinom1;

	EXPECT_TRUE(polinom1 == polinom2);
}

TEST(TPolinom, can_assign_empty_polynom)
{
	TPolinom polinom1;
	TPolinom polinom2;

	polinom2 = polinom1;

	EXPECT_TRUE(polinom1 == polinom2);
}

TEST(TPolinom, can_add_polynoms)
{
	const int size = 1;
	int mon1[][2] = { { 2, 1 } };
	int mon2[][2] = { { 1, 1 } };
	// 2z
	TPolinom polinom1(mon1, size);
	// z
	TPolinom polinom2(mon2, size);

	TPolinom Pol = polinom1 + polinom2;

	const int expected_size = 1;
	int expected_mon[][2] = { { 3, 1 } };
	// 3z
	TPolinom expected_Pol(expected_mon, expected_size);
	EXPECT_TRUE(Pol == expected_Pol);
}
